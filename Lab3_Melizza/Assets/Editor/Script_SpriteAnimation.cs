﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class Script_SpriteAnimation : EditorWindow
{
    public static Object selectedObject;

    int numAnimations;
    string controllerName;
    string[] animationNames = new string[100];
    float[] clipFrameRate = new float[100];
    float[] clipTimeBetween = new float[100];
    int[] startFrames = new int[100];
    int[] endFrames = new int[100];
    bool[] pingPong = new bool[100];
    bool[] loop = new bool[100];

    [MenuItem("Project Tools/ 2D Animation")]
    static void Init()
    {
        selectedObject = Selection.activeObject;

        if (selectedObject == null)
            return;

        Script_SpriteAnimation window = (Script_SpriteAnimation)EditorWindow.GetWindow(typeof(Script_SpriteAnimation));
        window.Show();
    }

    void OnGUI()
    {
        EditorGUILayout.LabelField("Animations for " + selectedObject.name);
        EditorGUILayout.Separator();

        controllerName = EditorGUILayout.TextField("Controller Name ", controllerName);
        numAnimations = EditorGUILayout.IntField("How many animations?", numAnimations);

        for (int i = 0; i < numAnimations; i++)
        {
            animationNames[i] = EditorGUILayout.TextField("Animation Name", animationNames[i]);

            EditorGUILayout.BeginHorizontal();
            startFrames[i] = EditorGUILayout.IntField("Start Frame", startFrames[i]);
            endFrames[i] = EditorGUILayout.IntField("End Frame", endFrames[i]);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            clipFrameRate[i] = EditorGUILayout.FloatField("Frame Rate", clipFrameRate[i]);
            clipTimeBetween[i] = EditorGUILayout.FloatField("Frane Spacing", clipTimeBetween[i]);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            loop[i] = EditorGUILayout.Toggle("Loop", loop[i]);
            pingPong[i] = EditorGUILayout.Toggle("PingPong", pingPong[i]);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Separator();
        }

        //create a button labeled "Create"
        if (GUILayout.Button("Create"))
        {
            //if the button has been pressed
            UnityEditor.Animations.AnimatorController controller = 
                UnityEditor.Animations.AnimatorController.CreateAnimatorControllerAtPath(("Assets/" + controllerName + ".controller"));

            for (int i = 0; i < numAnimations; i++)
            {
                //create animation clip
                AnimationClip tempClip = new AnimationClip();

                if (loop[i])
                {
                    AnimationClipSettings settings = AnimationUtility.GetAnimationClipSettings(tempClip);
                    settings.loopTime = true;
                    settings.loopBlend = true;
                    AnimationUtility.SetAnimationClipSettings(tempClip, settings);
                }

                controller.AddMotion(tempClip);
            }
        }
    }//end OnGUI()

    public AnimationClip CreateClip(Object obj, string clipName, int startFrame, int endFrame, float frameRate, float timeBetween, bool pingPong)
    {
        //get the path of the object
        string path = AssetDatabase.GetAssetPath(obj);

        //extract the sprites
        Object[] sprites = AssetDatabase.LoadAllAssetsAtPath(path);

        //determine how many frames, and the length of each frame
        int frameCount = endFrame - startFrame + 1;
        float frameLength = 1.0f / timeBetween;

        //create a new (empty) animation clip
        AnimationClip clip = new AnimationClip();

        //Set the framerate for the clip
        clip.frameRate = frameRate;

        //create the new (empty) curve binding
        EditorCurveBinding curveBinding = new EditorCurveBinding();

        //assign it to change the sprite renderer
        curveBinding.type = typeof(SpriteRenderer);

        //assign it to alter the sprite of the sprite renderer
        curveBinding.propertyName = "m_Sprite";

        //create a container for all of the keyframes
        ObjectReferenceKeyframe[] keyFrames;

        //determine how many frames there will be if we are or are not poingponging
        if (!pingPong)
            keyFrames = new ObjectReferenceKeyframe[frameCount + 1];
        else
            keyFrames = new ObjectReferenceKeyframe[frameCount * 2 + 1];

        //keep track of what frame number we are on
        int frameNumber = 0;

        //loop from start to end, incrementing frameNumber as we go
        for (int i = startFrame; i < endFrame; frameNumber++)
        {
            //create an empty keyFrame
            ObjectReferenceKeyframe tempKeyFrame = new ObjectReferenceKeyframe();
            //assign it to appear in the animation
            tempKeyFrame.time = frameNumber * frameLength;
            //assign it a sprite
            tempKeyFrame.value = sprites[i];
            //place it into the container for all the keyFrames
            keyFrames[frameNumber] = tempKeyFrame;
        }

        //If we are pingponging this animation
        if (pingPong)
        {
            //create keyframes starting at the end and going backwards
            //continue to keep track of the frame number
            for(int i = endFrame; i >= startFrame; i--, frameNumber--)
            {
                ObjectReferenceKeyframe temptKeyFrame = new ObjectReferenceKeyframe();
                temptKeyFrame.time = frameNumber * frameLength;
                temptKeyFrame.value = sprites[i];
                keyFrames[frameNumber] = temptKeyFrame;
            }
        }

        //create the last sprite to stop it from switching quickly from the last frame to the first one
        ObjectReferenceKeyframe lastSprite = new ObjectReferenceKeyframe();
        lastSprite.time = frameNumber * frameLength;
        lastSprite.value = sprites[startFrame];
        keyFrames[frameNumber] = lastSprite;

        //assign the name
        clip.name = clipName;

        //apply the curve
        AnimationUtility.SetObjectReferenceCurve(clip, curveBinding, keyFrames);

        //create the clip
        AssetDatabase.CreateAsset(clip, ("Asset/" + clipName + ".anim"));

        //return the clip
        return clip;
    }//end CreateClip()

    void OnFocus()
    {
        if (EditorApplication.isPlayingOrWillChangePlaymode)
            this.Close();
    }
}
